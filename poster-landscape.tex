\documentclass{beamer}
\usepackage[orientation=landscape,size=a0,scale=1.35]{beamerposter}
\mode<presentation>{\usetheme{UZH}}

\usepackage[T1]{fontenc}
\usepackage{newtxtext,newtxmath,microtype} % use newpx[text|math] for palatino instead of newtxtext for times
\usepackage{multirow,booktabs,array,tabularx}
\usepackage{url,ragged2e,graphicx}
\usepackage[font=scriptsize,justification=justified]{caption}
\usepackage{hyperref}

\urlstyle{same} %so it doesn't use a typewriter font for urls.
\graphicspath{{img/} {imglocal/} {./}}
\DeclareGraphicsExtensions{.pdf,.png,.jpg,.mps}
\renewcommand\familydefault{\sfdefault}


%%% TITLE INFO %%%

\title{Neuromorphic cortical models for decision-making, memory and learning}
\author{Maryada, Jingyue Zhao, Ioan-Iustin Fodorut, and Giacomo Indiveri\\University of Zurich and ETH Zurich, Institute of Neuroinformatics}%$^{1}$}
\institute[INI]{Institute of Neuroinformatics}%{$^{1}$Institute ...}
\email{giacomo@ini.uzh.ch}
\weburl{http://www.ini.uzh.ch/}
\date{September, 2022}


%%% Default column default width (0.3 for 3 cols, 0.22 for 4 cols, etc.)
\newlength{\colwidth}
\setlength{\colwidth}{0.32\paperwidth}


%%% POSTER %%%
\begin{document}

\begin{frame}[t]
  \begin{columns}[T]
    %%%%%%%%%%%%%% First column %%%%%%%%%%%%%%%%%%%%%%%%%%% 
    \begin{column}{\colwidth}
      
      %%% BOX %%%
      \begin{defaultbox}{Abstract}
        Computational neuroscience and artificial intelligence have made tremendous progress in recent years.
        However, today’s artificial systems are still not able to compete with biological ones.\\
        In our lab we combine advances in neural computation with the latest achievements in mixed-signal analog/digital neuromorphic technologies to build \alert{neuromorphic cognitive systems} that can interact intelligently with the environment in closed-loop real-time settings.\\
        We apply the models and systems developed to edge-computing applications in the fields of robotics and bio-medical signal processing.
      \end{defaultbox}

      %%% BOX %%%
      \begin{notebox}{Hardware aware cortical models of computation}
        We explore the computational features of recurrent cortical circuits, comprising cooperative-competitive and dis-inhibition circuit motifs, using both hardware-aware software models and neuromorphic VLSI processors.
        \begin{columns}
          \column{0.3\textwidth}
          \centering
          \includegraphics[width=0.8\textwidth]{circuit-motif}
          \column{0.7\textwidth}
          \includegraphics[width=\textwidth]{dynapse2-annotated}
        \end{columns}
        \color{uzh@orange100}\small\textbf{Cortical cells with different excitatory and inhibitory profiles are emulated in real-time on neuromorphic hardware.}
      \end{notebox}

      %%% BOX %%%
      \begin{defaultbox}{Dynamic Neuromorphic Asynchronous Processors}
        \noindent The DYNAP-SE2 is a second generation neuromorphic processor with analog neuron circuits and asynchronous digital routing circuits.
        \vspace*{1em}
        \begin{center}
          \begin{tabular}{@{}p{0.5\textwidth} p{0.4\textwidth}@{}}
            \toprule
            \midrule
            Process technology & 0.18\,$\mu$m CMOS\\
            Neuron type & AdExp-I\&F\\
            Number of neurons  & 1\,k in 4 cores\\
            Number of dendritic compartments & 4 per neuron\\
            Number of synapses & 64 per neuron\\
            Synapse types & 4 (GABA$_A$, GABA$_B$, AMPA, NMDA)\\
            \bottomrule
          \end{tabular}
        \end{center}
      \end{defaultbox}
      \vfill
    \end{column}
    
    
    %%%%%%%%%%%%%% Second column %%%%%%%%%%%%%%%%%%%%%%%%%%%
    \begin{column}{\colwidth}
      
      %%% BOX %%%
      \begin{defaultbox}{Silicon neuron circuits}
        \begin{columns}[b]
          \column{0.675\textwidth}
          \includegraphics[width=\textwidth]{neuron-fdsoi}
          \column{0.325\textwidth}
          \includegraphics[width=\textwidth]{data-neuron-refr-22nm}
        \end{columns}
        {\color{uzh@orange100}\small\textbf{Adaptive Exponential I\&F neuron circuit, designed for a 22\,nm FDSOI process.}}\\
%        \vspace*{1em}
        Mixed-signal silicon neuron circuits can reproduce complex dynamics of real neurons, using less than 15\,pJ/spike.
      \end{defaultbox}

      %%% BOX %%%
      \begin{defaultbox}{Spike-based learning simulations}
        \begin{columns}
          \column{0.45\textwidth}
          \centering
          \includegraphics[width=\textwidth]{learning-network}
          \column{0.55\textwidth}
          \centering
          \includegraphics[width=0.9\textwidth]{learning-results}
        \end{columns}
        {\color{uzh@orange100}\small\textbf{Homogeneous populations of excitatory neurons learn to form clusters tuned to different input stimuli.}}\\
        The circuit is composed of multiple compartmental pyramidal cells (PYR, red), inhibitory inter-neurons (PV, blue) forming a soft-Winner-take-all (WTA) network, and a dis-inhibitory motif formed by extra inhibitory inter-neurons (VIP, SST, blue).\\
        Target (teacher) projections arrive selectively at the apical compartments of sub-sets of PYR neurons, while input (e.g., sensory) stimuli project to the basal compartments of all PYR neurons.
        In absence of any teacher signal, dis-inhibitionn is released and plasticity is suppressed.\\
        Before learning (yellow region), neurons are not tuned to any input.\\
        During learning (red region), neurons differentiate depending on which teacher is presented with which pattern.\\
        After learning (blue region), the neurons show selectivity to the different input patterns and do not change their weights anymore.
      \end{defaultbox}
      \vfill
    \end{column}
    
    
    %%%%%%%%%%%%%% Third column %%%%%%%%%%%%%%%%%%%%%%%%%%% 
    \begin{column}{\colwidth}

      %%% BOX %%%
      \begin{defaultbox}{Working memory demonstration}
        \includegraphics[width=0.5\textwidth]{nest-dynapse} \hfill \\
        \vspace*{-30pt}
        \hfill \includegraphics[width=0.75\textwidth]{wta-dynapse2}\\
        {\color{uzh@orange100}\small\textbf{Winner-take-all network emulated on the DYNAP-SE2 hardware.}}\\
        
        Two clusters of excitatory neurons compete via a global inhibitory pool.
        Each cluster comprises 32 neurons (blue and orange) recurrently connected.
        The inhibitory pool comprises 16 inhibitory neurons (red) which inhibit both excitatory clusters. 
        Grey dots represent an external stimulis sent to 4 input neurons.
        % The stimulus is presented only for a short duration (ON-phase).
        In absence of external stimuli the recurrent network dynamics of the excitatory cluster maintains persistent activity.
        As new stimuli are presented the populations compete and eventually switch state. 
      \end{defaultbox}
      
      %%% BOX %%%
      \begin{alertbox}{Conclusions}
        By constraining and validating computational neuroscience models, emulations of cortical circuits in neuromorphic hardware allow us to gain further insights in the mechanisms used by neural circuits to compute reliably using a noisy and highly variable computing substrate.
        Progress in neuroscience is instrumental for guiding the design of novel neuromorphic computing architectures that can be used in a wide range of real-world application domains.
      \end{alertbox}
      \vspace{2em}
      
      %%% Acknowledgments %%%
      \begin{columns}
        \column{0.5\textwidth}
        \includegraphics[width=\textwidth]{logo-snf}
        \column{0.1\textwidth}
        \includegraphics[width=\textwidth]{logo-erc}
        \column{0.3\textwidth}
        \includegraphics[width=\textwidth]{logo-h2020-long}
      \end{columns}
    \end{column}
  
  \end{columns}
\end{frame}

\end{document}
