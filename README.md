# uzhposter_template_latex

Latex beamerposter template for UZH and joint UZH ETH institutes.
Based on the UZH templates provided on the [UZH corporate design site](https://www.cd.uzh.ch/de/vorlagen/wissenschaftliches-poster.html)

- [ ] poster-portrait: two columns, portrait mode
- [ ] poster-landscape: three columns, landscape mode

